class ModelUsingShortUrl < ActiveRecord::Base
  belongs_to ...

  serialize :short_url, ShortUrl::Serializer.new
  validate :short_url_is_valid
  before_validation :generate_short_url_if_necessary

  ...

  private

    def generate_short_url_if_necessary
      return if short_url.present?

      base_url = "http://#{SHORT_URL_DOMAIN}"
      self.short_url = ShortUrl::Generator.new(base_url: base_url).generate.url
    end

    def short_url_is_valid
      return if short_url.blank?

      valid_short_url_domains = [*ShortUrl::GENERIC_SHORT_DOMAINS, domain_for_short_url]

      unless valid_short_url_domains.include? short_url.domain
        errors.add :short_url, 'domain is invalid'
      end
      unless short_url.valid?
        short_url.append_errors self, :short_url
      end
    end

end
