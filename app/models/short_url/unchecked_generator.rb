class ShortUrl
  class UncheckedGenerator < Generator
    SHORTCUT_EPOCH = Time.utc 2016,3,1

    attr_reader :api_key

    def initialize api_key: nil, random_size: 2, **other_args
      super other_args.merge(random_size: random_size)
      self.api_key = api_key
    end

    def generate
      ShortUrl.new generate_url
    end

    def generate_id
      # 10 characters identifier with generic domain
      # +------------------+----------------+--------------------------+
      # | api key start    | random number  | seconds since epoch      |
      # | (3 chars)        | (2 chars)      | (5 chars)                |
      # +------------------+----------------+--------------------------+
      #
      # 7 characters identifier with custom domain
      # +------------------+----------------+--------+
      # |  random number  | seconds since epoch      |
      # |  (2 chars)      | (5 chars)                |
      # +------------------+----------------+--------+
      random = random()
      id = []
      id << encode_api_key_with_random(random) unless custom_domain?
      id += [encode_random(random), encoded_time]
      id.join
    end

    def encode_api_key_with_random random
      rotate api_key, random
    end

    def encoded_time
      lpad encode(seconds_since_epoch), padding_size: 5
    end

    def encode_random random
      lpad encode(random), padding_size: random_size
    end

    def seconds_since_epoch
      Integer(Time.now - SHORTCUT_EPOCH)
    end

    def unrandomize_key_part short_url
      identifier_key    = short_url.identifier[0,API_KEY_LENGTH]
      identifier_random = short_url.identifier[API_KEY_LENGTH, random_size]
      rotate identifier_key, decode(identifier_random) * -1
    end

    def rotate string, num
      max_value = encoder.base_num ** string.length
      decoded = decode string
      if decoded + num < 0
        value = max_value + decoded + num
      else
        value = decoded + num
      end
      lpad encode(value % max_value), padding_size: string.length
    end

    private

      def api_key= key
        @api_key = key[0,API_KEY_LENGTH] unless key.nil?
      end

      def custom_domain?
        ShortUrl.new(base_url).custom_domain?
      end
  end

end
