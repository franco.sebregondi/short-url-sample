# ShortUrl::Encoder encodes/decodes int's to another base.
# For ShortUrls an alphanumeric base is used (A-Z,a-z,0-9).
class ShortUrl
  class Encoder
    attr_accessor :base, :base_num
    attr_reader :radix

    def initialize base = ('0'..'9').to_a + ('a'..'z').to_a + ('A'..'Z').to_a
      self.base = base
      self.base_num = base.size
    end

    def encode int
      Radix::Base.new(base).convert(int, 10)
    end

    # This is a manual version of the encoder
    # We keep this version here as a reference implementation for mobile
    def _encode int
      exp = 0
      begin
        exp += 1
        div = int / (base_num ** exp)
        if exp > 1
          (int - (base_num ** exp) +1 ) % base_num
        else
          (int - (base_num ** exp)) % base_num
        end
      end while div > 0
    end

    def decode string
      string.chars.map{ |c| base.index c }.b(base_num).to_i
    end

    private
      def index c
        base.index c
      end
  end
end

