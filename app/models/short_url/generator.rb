class ShortUrl
  class Generator

    DEFAULT_BASE_URL = "http://#{ShortUrl::DEFAULT_SHORT_DOMAIN}"

    attr_reader :encoder, :base_url, :random_size

    def initialize base_url: nil, encoder: nil, random_size: 6
      self.encoder = encoder || Encoder.new
      self.base_url = base_url || DEFAULT_BASE_URL
      self.random_size = random_size
    end

    def generate
      begin
        url = ShortUrl.new generate_url
      end while ShortLink.where(short_url: url.to_s).exists?
      url
    end

    def generate_id
      lpad encode(random), padding_size: random_size
    end

    def generate_url
      "#{base_url}/#{generate_id}"
    end

    private
      attr_writer :encoder, :base_url, :random_size

      def random
        rand(1..(encoder.base_num ** random_size))
      end

      def encode input
        encoder.encode input
      end

      def decode input
        encoder.decode input
      end

      def lpad input, padding_sign: '0', padding_size: 0
        input.rjust padding_size, padding_sign
      end

  end
end


