class ShortUrl

  class Serializer
    def dump value
      value.respond_to?(:url) ? value.url : value
    end

    def load value
      ShortUrl.new value if value.present?
    end
  end

  include ActiveModel::Validations

  DEFAULT_SHORT_DOMAIN = SHORT_SHORTCUT_SERVICE
  GENERIC_SHORT_DOMAINS = [DEFAULT_SHORT_DOMAIN, 'get.sc']
  API_KEY_LENGTH = 3
  CHECKED_IDENTIFIER_LENGTH = 6
  UNCHECKED_IDENTIFIER_LENGTH = 7..10

  delegate :blank?,   to: :url
  delegate :present?, to: :url
  delegate :to_s,     to: :url
  delegate :inspect,  to: :url
  delegate :as_json,  to: :url

  validates :url, http_url: true
  validate :url_validations

  attr_reader :url

  def initialize url
    self.url = url
  end

  def checked?
    @checked ||= identifier.length <= CHECKED_IDENTIFIER_LENGTH
  end

  def identifier
    @identifier ||= parsed_url.path[1..-1]
  end

  def custom_domain?
    !GENERIC_SHORT_DOMAINS.include? domain
  end

  def domain
    parsed_url.host
  end

  # returns the api_key part that was encoded in the api
  def api_key_part
    unless checked? || custom_domain?
      generator = ShortUrl::UncheckedGenerator.new
      generator.unrandomize_key_part self
    end
  end

  def append_errors record, field
    record.errors.add field, errors[:url]
  end

  def ==(o)
    case o
    when String
      o == url
    else
      o.class == self.class && o.url == url
    end
  end

  # I know, it is bad practice to implement to_str on
  # non-string classes. But it is so convenient.
  def to_str
    url
  end

  private
    attr_writer :url

    def url_validations
      return unless parsed_url
      errors.add :url, 'must not include query' if parsed_url.query
      errors.add :short_url, 'must not include fragment' if parsed_url.fragment
    end

    def parsed_url
      @parsed_url ||= Addressable::URI.parse(url) rescue nil
    end
end
