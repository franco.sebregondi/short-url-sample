require "test_helper"

describe "ShortUrl::Generator model" do
  let(:hex_encoder) { ShortUrl::Encoder.new %w( 0 1 2 3 4 5 6 7 8 9 a b c d e f ) }

  subject { ShortUrl::Generator.new }

  describe "#generate" do
    subject { ShortUrl::Generator.new(encoder: hex_encoder) }
    it "returns a ShortUrl" do
      subject.generate.must_be_kind_of ShortUrl
    end

    it "is marked as checked" do
      subject.generate.must_be :checked?
    end

    it "has a newly url generated" do
      subject.stub :generate_id, 'hash' do
        subject.generate.url.must_equal "#{subject.base_url}/hash"
      end
    end
  end

  describe "#generate_id" do
    it "generates a random identifier with length random_size" do
      subject.generate_id.size.must_equal subject.random_size
    end
    it "generates the identifier with padding if random is too short" do
      subject.stub(:random, 74737) do
        subject.generate_id.must_equal "000jrr"
      end
    end
  end
end

