require "test_helper"

describe "ShortUrl::UncheckedGenerator model" do
  let(:hex_encoder) { ShortUrl::Encoder.new %w( 0 1 2 3 4 5 6 7 8 9 a b c d e f ) }

  describe "#encoded_time" do
    subject { ShortUrl::UncheckedGenerator.new(encoder: hex_encoder) }
    it "returns the encoded time" do
      subject.stub :seconds_since_epoch, 0 do
        subject.encoded_time.must_equal '00000'
      end
      subject.stub :seconds_since_epoch, 16 do
        subject.encoded_time.must_equal '00010'
      end
    end
  end

  describe "#encode_random" do
    subject { ShortUrl::UncheckedGenerator.new(encoder: hex_encoder) }
    it "returns the encoded random" do
      subject.encode_random(12).must_equal '0c'
      subject.encode_random(16).must_equal '10'
    end
  end

  describe "#encode_api_key_with_random" do
    subject { ShortUrl::UncheckedGenerator.new(encoder: hex_encoder) }
    it "returns the encoded api key" do
      subject.stub :api_key, 'abc' do
        subject.encode_api_key_with_random(0).must_equal 'abc'
      end
      subject.stub :api_key, 'abc' do
        subject.encode_api_key_with_random(5).must_equal 'ac1'
      end
    end
  end

  describe "#rotate" do
    subject { ShortUrl::UncheckedGenerator.new(encoder: hex_encoder) }

    it "rotates a string by n characters" do
      subject.rotate('00', 4).must_equal '04'
      subject.rotate('ff', 1).must_equal '00'
      subject.rotate('ff', 4).must_equal '03'
      subject.rotate('03', -4).must_equal 'ff'
    end
  end

  describe "#generate" do
    let(:api_key) { 'abcdefg' }
    subject { ShortUrl::UncheckedGenerator.new(api_key: api_key, encoder: hex_encoder) }

    it "returns a ShortUrl" do
      subject.generate.must_be_kind_of ShortUrl
    end

    it "is marked as unchecked" do
      subject.generate.wont_be :checked?
    end

    it "has a newly url generated" do
      subject.stub :generate_id, 'hash' do
        subject.generate.url.must_equal "#{subject.base_url}/hash"
      end
    end
  end

  describe "#generate_id" do
    let(:api_key) { 'abcdefg' }
    let(:encoder) { ShortUrl::Encoder.new }
    subject { ShortUrl::UncheckedGenerator.new(api_key: api_key, encoder: encoder) }

    describe "without a custome domain" do
      it "is 10 chars long" do
        subject.generate_id.size.must_equal 10
      end
    end
    describe "with a custome domain" do
      it "is 7 chars long" do
        base_url = "http://short.li"
        generator = ShortUrl::UncheckedGenerator.new(api_key: api_key, base_url: base_url, encoder: encoder)
        generator.generate_id.size.must_equal 7
      end
    end
  end

  describe "#rotate" do
    subject { ShortUrl::UncheckedGenerator.new(encoder: hex_encoder) }

    it "rotates a string by n characters" do
      subject.rotate('00', 4).must_equal '04'
      subject.rotate('ff', 1).must_equal '00'
      subject.rotate('ff', 4).must_equal '03'
      subject.rotate('03', -4).must_equal 'ff'
    end
  end

  describe "#unrandomize_key_part" do
    let(:api_key) { 'abcdefg' }
    subject { ShortUrl::UncheckedGenerator.new(api_key: api_key) }

    it "returns the unrandomized_key part" do
      short_url = subject.generate
      subject.unrandomize_key_part(short_url).must_equal api_key[0, ShortUrl::API_KEY_LENGTH]
    end
  end
end
