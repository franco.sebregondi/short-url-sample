require "test_helper"

describe ShortUrl::Encoder do

  subject { ShortUrl::Encoder.new ['a', 'b', 'c', 'd', 'e', 'f'] }

  describe "encoding/decoding" do
    it "encodes correctly" do
      subject.encode(0).must_equal 'a'
      subject.encode(1).must_equal 'b'
      subject.encode(2).must_equal 'c'
      subject.encode(3).must_equal 'd'
      subject.encode(4).must_equal 'e'
      subject.encode(5).must_equal 'f'
      subject.encode(6).must_equal 'ba'
      subject.encode(7).must_equal 'bb'
      subject.encode(8).must_equal 'bc'
      subject.encode(12).must_equal 'ca'
      subject.encode(35).must_equal 'ff'
      subject.encode(36).must_equal 'baa'
      subject.encode(37).must_equal 'bab'
      subject.encode(38).must_equal 'bac'
      subject.encode(215).must_equal 'fff'
      subject.encode(216).must_equal 'baaa'
      subject.encode(1296).must_equal 'baaaa'
      subject.encode(7776).must_equal 'baaaaa'
    end
    it "encodes to Base62" do
      base62 = ShortUrl::Encoder.new ('0'..'9').to_a + ('a'..'z').to_a + ('A'..'Z').to_a
      digits = base62.base_num

      base62.encode(digits ** 0 - 1).must_equal '0'
      base62.encode(digits ** 1 - 1).must_equal 'Z'
      base62.encode(digits ** 1 - 0).must_equal '10'
      base62.encode(digits ** 2 - 1).must_equal 'ZZ'
      base62.encode(digits ** 2 - 0).must_equal '100'
      base62.encode(digits ** 3 - 1).must_equal 'ZZZ'
      base62.encode(digits ** 3 - 0).must_equal '1000'
      base62.encode(digits ** 4 - 1).must_equal 'ZZZZ'
      base62.encode(digits ** 4 - 0).must_equal '10000'
      base62.encode(digits ** 5 - 1).must_equal 'ZZZZZ'
    end

    it "decodes from Base62" do
      base62 = ShortUrl::Encoder.new ('0'..'9').to_a + ('a'..'z').to_a + ('A'..'Z').to_a
      digits = base62.base_num

      base62.decode('0').must_equal digits ** 0 - 1
      base62.decode('Z').must_equal digits ** 1 - 1
      base62.decode('10').must_equal digits ** 1 - 0
      base62.decode('ZZ').must_equal digits ** 2 - 1
      base62.decode('100').must_equal digits ** 2 - 0
      base62.decode('ZZZ').must_equal digits ** 3 - 1
      base62.decode('1000').must_equal digits ** 3 - 0
      base62.decode('ZZZZ').must_equal digits ** 4 - 1
      base62.decode('10000').must_equal digits ** 4 - 0
      base62.decode('ZZZZZ').must_equal digits ** 5 - 1
    end

    it "encodes/decodes to Base62" do
      input = 573628
      encoded_string = subject.encode input
      decoded_int = subject.decode encoded_string
      decoded_int.must_equal input
    end

    it "encodes to hex" do
      base16 = ShortUrl::Encoder.new %w( 0 1 2 3 4 5 6 7 8 9 a b c d e f )
      base16.encode(0).must_equal '0'
      base16.encode(15).must_equal 'f'
      base16.encode(16).must_equal '10'
      base16.encode(255).must_equal 'ff'
    end

    it "decodes to hex" do
      base16 = ShortUrl::Encoder.new %w( 0 1 2 3 4 5 6 7 8 9 a b c d e f )
      base16.decode('0').must_equal 0
      base16.decode('f').must_equal 15
      base16.decode('ff').must_equal 255
    end
  end
end

