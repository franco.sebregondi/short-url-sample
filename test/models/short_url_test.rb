require "test_helper"

describe ShortUrl do

  describe "#identifier" do
    it "returns the identifer part of the url" do
      ShortUrl.new("http://goo.gl/ab35421d").identifier.must_equal 'ab35421d'
    end
  end

  describe "#custom_domain?" do
    it "returns true for urls with a custom domain" do
      ShortUrl.new('http://scm.st/ycKSj0Anoz').wont_be :custom_domain?
      ShortUrl.new('https://get.sc/Sj0Anoz').wont_be :custom_domain?
      ShortUrl.new('http://link.st/Sj0Anoz').must_be :custom_domain?
    end
  end

  describe "#checked?" do
    it "determines if a short url is checked/unchecked based on the url length" do

      # identifier size is less or equal 6
      ShortUrl.new('http://scm.st/gGp79b').must_be :checked?

      # identifier length > 6
      ShortUrl.new('https://get.sc/Sj0Anoz').wont_be :checked?
    end
  end

  describe "#api_key_part" do
    it "returns the start of the api_key" do
      ShortUrl.new('http://scm.st/ycKSj0Anoz').api_key_part.must_equal 'xkr'
    end
    it "returns nil if short url has a custom domain" do
      ShortUrl.new('http://vivi.no/Sj0Anoz').api_key_part.must_be_nil
    end
    it "returns nil if short url has been checked" do
      ShortUrl.new('http://vivi.no/j0Anoz').api_key_part.must_be_nil
    end
  end

  describe 'validations' do
    describe 'url' do
      it 'must validate the format' do
        ShortUrl.new('http://scm.st/123').must_be :valid?
        ShortUrl.new('invalid url').wont_be :valid?
        ShortUrl.new('mailto:foo@bar.com').wont_be :valid?
      end
      it 'must validate absence of query/fragment' do
        ShortUrl.new('http://scm.st/123').must_be :valid?
        ShortUrl.new('http://scm.st/123?foo=bar').wont_be :valid?
      end
    end
  end
end


